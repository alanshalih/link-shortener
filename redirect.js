const fastify = require('fastify')({
    logger: false
})

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

const UserAgent = require("ua-parser-js");

const Reader = require("@maxmind/geoip2-node").Reader;

require('dotenv').config()

var knex =  require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        charset : 'utf8mb4'
    }
});

const asyncRedis = require("async-redis");
const client = asyncRedis.createClient();

fastify.register(require('fastify-cookie'), {
    secret: "khilafah mantap", // for cookies signature
    parseOptions: {}     // options for parsing cookies
  })
  

const {
    Model
} = require('objection');
Model.knex(knex);


class Link extends Model {
    static get tableName() {
        return 'links';
    }
}

class UrlStat extends Model {
    static get tableName() {
        return 'url_stats';
    }
}

// Declare a route
fastify.get('/', async (request, reply) => {
    reply.redirect(process.env.APP_URL)
})

fastify.get('/check', async (request, reply) => {
    return 'ok'
})

fastify.get('/cookie', (req, reply) => { 
    reply 
      .cookie('baz', 'baz') // alias for setCookie
      .setCookie('bar', 'bar', {
        path: '/',
        signed: true
      })
      .send({ hello: 'world' })
  })

async function getCountryByIp(ip) {
    try {
        const reader = await Reader.open("database/GeoLite2-Country.mmdb");
        const country = await reader.country(ip);

        return {
            country_code: country.country.isoCode,
            country_name: country.country.names.en
        };
    } catch (error) {
        return {
            country_code: "N/A",
            country_name: "Unknown"
        };
    }
}




async function LogTraffic(link, request)
{
        await Link.query().findById(link.id).increment("click",1);

        const agent = UserAgent(request.headers['user-agent']);
        const country = await getCountryByIp(request.ip);
        await UrlStat.query().insert({
            link_id: link.id,
            referer: request.headers["referer"] ? request.headers["referer"] : null,
            ip: request.ip,
            device: agent.device.vendor ? agent.device.vendor : "Other",
            platform: agent.os.name,
            platform_version: agent.os.version,
            browser: agent.browser.name,
            browser_version: agent.browser.version,
            country: country.country_code,
            country_full: country.country_name
        });

}

const axios = require('axios')

 
 



 
async function checkLink(link)
{
    let all = JSON.parse(link.multi_url)

    let domains =  all.map(item=>{return new URL(item).host}).join('%0A')

    console.log(domains)
 

    axios.post("https://trustpositif.kominfo.go.id/Rest_server/getrecordsname_home",{name : domains}).then(  async response=>{
          
    //    let data = response.data.values.filter(item=>{
    //          return item.Status == 'Tidak Ada'
    //      });
        let obj = {};

        response.data.values.forEach(item=>{
            obj[item.Domain] = item.Status == 'Tidak Ada';
        })
          
         let result = all.filter(item=>{
            return obj[new URL(item).host];
        })
 

        if(result.length < domains.length)
        {
            await Link.query().findById(link.id).patch({multi_url : JSON.stringify(result)})

            await client.del("link:"+link.slug);
        }
        

        await client.setex("checkLink:"+link.slug,60*10,"ok");
     

       
         

    },async error=>{
        console.log(error)
      

 

    })
}

function generateLink(link, other = null)
{
    if(link.type == 'url')
    return link.url;

    if(link.type == 'multi-url')
    {
        var urls = JSON.parse(link.multi_url);
        return urls[Math.floor(Math.random() * urls.length)];
    }

    if(link.type == 'share_wa')
    return "https://api.whatsapp.com/send?text="+encodeURIComponent(link.text);

    if(link.type == 'contact_me')
    return "https://api.whatsapp.com/send?phone="+link.phone+"text="+encodeURIComponent(link.text);

    if(link.type == 'by_pass')
    {
        return link.url+'/'+link.slug+'/'+other;
    }
}

fastify.post('/remove-ekspo-rajab', async (request, reply) => {
    const link = await Link.query().findById(request.body.id);

    let multi_url = JSON.parse(link.multi_url);

    const findIndex = multi_url.findIndex(item=>{
        return item.includes(request.body.phone)
    })

    if(findIndex > -1)
    {
        multi_url.splice(findIndex,1)
    } 
    await Link.query()
    .findById(request.body.id)
    .patch({multi_url : JSON.stringify(multi_url)} );

    await client.del("link:"+request.params.slug);

    reply.send("ok")
})


fastify.post('/add-ekspo-rajab', async (request, reply) => {
    const link = await Link.query().findById(request.body.id);

    let multi_url = JSON.parse(link.multi_url);

    
    const findIndex = multi_url.findIndex(item=>{
        return item.includes(request.body.phone)
    })

    if(findIndex == -1)
    {
        multi_url.push("https://wa.me/"+request.body.phone+"?text=Konfirmasi%20Nomor%20WA%20EkspoRajab")
    } 

   
    await Link.query()
    .findById(request.body.id)
    .patch({multi_url : JSON.stringify(multi_url)} );

    await client.del("link:"+request.params.slug);

    reply.send("ok")
})



fastify.get('/:slug', async (request, reply) => {
    const value = await client.get("link:"+request.params.slug);

  
    const check =  await client.get("checkLink:"+request.params.slug);
  
    if(value)
    {
        var link = JSON.parse(value);
     
        LogTraffic(link,request)

        let result = await request.cookies['link'+request.params.slug]  
         

        if(!result)
        {
            result = generateLink(link);
        } 

        if(link.check_link && link.type == 'multi-url'  && !check)
        {
            checkLink(link,result) 
        }

        const cookies_times = link.cookies_time ? link.cookies_time : 10;
        

        reply.cookie('link'+request.params.slug, result,{  expires: Date.now() + 1000*60*cookies_times }).redirect(result);

    }else{
        var link = await Link.query()
        .where('slug', request.params.slug).first();

        if(link)
        {   
            LogTraffic(link,request)  

            const cookies_times = link.cookies_time ? link.cookies_time : 10;
            
            await client.set("link:"+request.params.slug,JSON.stringify(link))

            let result = generateLink(link); 

            reply.cookie('link'+request.params.slug, result,{  expires: Date.now() + 1000*60*cookies_times }).redirect(result);
        }else{
            reply
            .code(404)
            .send('Link tidak ditemukan')
        }
    }
    
})

fastify.get('/:slug/:other', async (request, reply) => {
    const value = await client.get("link:"+request.params.slug);
    if(value)
    {
        var link = JSON.parse(value);   
        LogTraffic(link,request)
        reply.redirect(generateLink(link, request.params.other));
    }else{
        var link = await Link.query()
        .where('slug', request.params.slug).first();

        if(link)
        {   
            LogTraffic(link,request)
            client.set("link:"+request.params.slug,JSON.stringify(link))
            reply.redirect(generateLink(link, request.params.other));
        }else{
            reply
            .code(404)
            .send('Link tidak ditemukan')
        }
    }
    
})

// Run the server!
const start = async () => {
    try {
        await fastify.listen(23001)
        fastify.log.info(`server listening on ${fastify.server.address().port}`)
    } catch (err) {
        console.log(err)
        fastify.log.error(err) 
    }
}
start()