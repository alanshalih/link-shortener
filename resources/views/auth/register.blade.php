@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    @include('layouts.headers.guest')

    <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-secondary shadow border-0">
                  
                    <div class="card-body px-lg-5 py-lg-5">
                      
                            <h1>Sorry, we are close right now.</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
