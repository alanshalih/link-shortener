@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')

<div id="create-link" class="container mt--7">

    <div class="row mt-5">
        <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 mb-5 mb-xl-0">
            <div class="card shadow">
                @if(isset($link))
            <form action="/link/{{$link->id}}" method="POST">
                    @method('put')
                @else 
                <form action="/link" method="POST">
                @endif
                    @csrf
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"> <a href="/home" class="btn btn-sm mr-3 btn-outline-primary"><i
                                        class="fas fa-arrow-left"></i></a> Buat Link Baru</h3>
                        </div>
                        <div class="col text-right">
                          
                        </div>
                    </div>
                </div>
              
                <div class="container pa-3">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div class="form-group">
                        <label for="">Type Link</label>
                        <select v-model="type" name="type" required id="" class="form-control">
                            <option value="url">Link Shortener</option>
                            <option value="multi-url">Multi URL</option>
                            <option value="share_wa">Share ke WA</option>
                            <option value="contact_me">Kontak Saya (WA Chat)</option>
                            <option value="by_pass">By Pass</option>
                        </select>
                    </div>
                    <!-- Projects table -->
                    <div class="form-group">
                        <label for="">Nama Link</label>
                        <input name="title" v-model="title" @change="changeSlug()" placeholder="Judul Link ini adalah..." required
                            type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">URL Pendek</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">krm.li/</span>
                            </div>
                            <input v-model="slug" @change="checklink()" required type="text" class="form-control" name="slug" placeholder="url-kamu">
                        </div>
                        <small class="text-danger" v-if="url_exist">url ini sudah digunakan</small>
                    </div>

                
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                    <div v-if="type == 'url'" class="form-group">
                        <label for="">URL Tujuan</label>
                        <input name="url" value="{{$link->url ?? ''}}" placeholder="http://google.co.id" required type="text" class="form-control">
                    </div>
                    <div v-if="type == 'by_pass'" class="form-group">
                        <label for="">Domain Tujuan</label>
                        <input name="url" value="{{$link->url ?? ''}}" placeholder="http://google.co.id" required type="text" class="form-control">
                    </div>
                    <div v-if="type == 'multi-url'" class="form-group">
                        <label for="">Lama Cookie</label>
                        <input name="cookies_time" value="{{$link->cookies_time ?? ''}}" placeholder="10" required type="text" class="form-control">
                    </div>
                    <div v-if="type == 'multi-url'">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Bulk Input</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Single Input</a>
  </li> 
</ul>
<div class="tab-content mt-3" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  <div>
    <label for="">URL Tujuan</label>
                        <textarea class="form-control"  @change="evalUrl()" v-model="multi_url_string" id="" cols="30" rows="10"></textarea>
    </div>

  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  <div v-for="(url,index) in multi_url" class="form-group">
                        <label for="">URL Tujuan @{{index+1}}</label>
                     <div class="input-group">
                     <input v-model="multi_url[index]" @change="reverseEval()" placeholder="http://google.co.id" required type="text" class="form-control">
                     <div class="input-group-append">
                        <span @click="multi_url.splice(index,1);reverseEval()" class="btn btn-outline-danger" type="button"><i class="fas fa-trash"></i></span>
                    </div>
                     </div>
                        </div>
                        <span class="btn btn-primary btn-sm" @click="multi_url.push('');reverseEval()">Tambah URL</span>
  </div> 
</div>
   
                    
                      
                        <div class=" mb-4 mt-3">
                            <input type="checkbox" id="Check-Link" v-model="check_link" name="check_link">
                           <label for="Check-Link"> Check Link</label>
                        </div>
                    </div>
                    <input type="hidden" :value="JSON.stringify(multi_url)" name="multi_url">
                    <div v-if="type == 'contact_me'" class="form-group">
                        <label for="">Nomor WA Saya</label>
                        <input value="{{$link->phone ?? ''}}" name="phone" placeholder="62813522323223" required type="tel" class="form-control">
                        <small>harus didahului 62 (tanpa +)</small>
                    </div>
                    <div v-if="type == 'share_wa' || type == 'contact_me'" class="form-group">
                        <label for="">Text Pesan</label>
                        <textarea v-model="text" placeholder="Isi Pesan yang mau dikirim" required name="text" class="form-control" id=""
                            cols="30" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn mb-3 btn-block btn-lg btn-primary">Simpan</button>
                </div>
            </form>
            </div>
        </div>

    </div>

    @include('layouts.footers.auth')
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script>
    new Vue({
        el: '#create-link',
        data: {
            mode : 'bulk',
            type: '{{$link->type ?? "url"}}',
            slug: '{{$link->slug ?? ""}}',
            text: `{{$link->text ?? ""}}`,
            title: '{{$link->title ?? ""}}',
            id : '{{$link->id ?? ""}}',
            multi_url : [''],
            multi_url_string : '',
            check_link : parseInt('{{$link->check_link ?? ""}}'),
            url_exist : false
        },
        mounted() {
           @if(isset($link->multi_url))
                this.multi_url = {!! $link->multi_url !!}
                this.multi_url_string = this.multi_url.join('\n')
           @endif
        },
        methods: {
            evalUrl(){ 
                this.multi_url = this.multi_url_string.split('\n')
            },
            reverseEval(){
                this.multi_url_string = this.multi_url.join('\n')
            },
            string_to_slug(str) {
                str = str.replace(/^\s+|\s+$/g, ''); // trim
                str = str.toLowerCase();

                // remove accents, swap ñ for n, etc
                var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
                var to = "aaaaeeeeiiiioooouuuunc------";
                for (var i = 0, l = from.length; i < l; i++) {
                    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                    .replace(/\s+/g, '-') // collapse whitespace and replace by -
                    .replace(/-+/g, '-'); // collapse dashes

                return str;
            },
            changeSlug() {
                if(!this.id)
                this.slug = this.string_to_slug(this.title)
            },
            checklink(){
                axios.post('/checklink',{slug : this.slug}).then(response=>{
                    this.url_exist = response.data;
                    
                })
            }
        }
    })

</script>
@endpush
