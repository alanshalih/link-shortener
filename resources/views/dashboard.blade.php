@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid mt--7">
        
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Link Kamu</h3>
                            </div>
                            <div class="col text-right">
                                <a href="/link/create" class="btn btn-sm btn-primary">Tambah Baru</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col">Tujuan</th>
                                    <th scope="col">Klik</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($links as $link)

                                <tr>
                                    <th scope="row">
                                        <a href="/link/{{$link->id}}/edit">  {{$link->title}}</a>
                                    </th>
                                    <td>
                                        <a href="//mbo.is/{{$link->slug}}" target="_blank">mbo.is/{{$link->slug}}</a>
                                    <button data-trigger="click" onclick="hideTooltip()" data-toggle="tooltip" data-placement="top" title="Link telah disalin" data-clipboard-text="https://krm.li/{{$link->slug}}" class="btn btn--copy btn-sm btn-outline-success"><i class="fas fa-copy"></i></button>
                                    </td>
                                    <td>
                                       @if($link->type == 'url')
                                       {{$link->url}}
                                       @elseif($link->type == 'share_wa')
                                       share ke wa
                                       @else
                                       {{$link->phone}}
                                       @endif
                                    </td>
                                    <td>
                                        {{$link->click}}
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        {{ $links->links() }}
                    </div>
                </div>
            </div>
          
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
<script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
<script>
    new ClipboardJS('.btn--copy');
    function hideTooltip(){
        setTimeout(()=>{
            $('.btn--copy').tooltip('hide')
        },1000)
    }
</script>
@endpush