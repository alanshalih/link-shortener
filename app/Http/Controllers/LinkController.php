<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Cache;
use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $links = Link::where('user_id',Auth::id())->latest()->paginate(100);
        return view('dashboard',compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $link = Link::where('slug',$request->slug)->first();

        if($link)
        {
            return back()->withStatus(__('Maaf, Url Pendek ini sudah digunakan')); 
        }

        Redis::del("link:".$request->slug);

        $data = $request->except(['_token']);

        if(isset($data["check_link"]))
        {
            $data["check_link"] = 1;
        }

        Link::create($data);
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, Request $request)
    {
        //
        $link = Link::where('slug',$slug)->first();

        if($link)
        {
        
            if($request->test)
            {
                return $link;
            }
            DB::table('links')->where('id',$link->id)->increment('click');

            if($link->type == 'url')
            return redirect($link->url);
            

            if($link->type == 'share_wa')
            return redirect('https://api.whatsapp.com/send?text='.rawurlencode($link->text));

            if($link->type == 'contact_me')
            return redirect('https://api.whatsapp.com/send?phone='.$link->phone.'&text='.rawurlencode($link->text));


        }else{
            abort(404);
        }
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $link = Link::where('id',$id)->first();

        
        return view('link.create',compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Redis::del("link:".$request->slug);

        $data = $request->except(['_token','_method']);

      

        if(isset($data["check_link"]))
        {
            $data["check_link"] = 1;
        }else{
            $data["check_link"] = 0;
        }



        Link::where('id',$id)->update($data);
   
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkLink(Request $request)
    {
        return Link::where('slug',$request->slug)->count();
    }

    public function redirectIntoRamadhankita(Request $request)
    {
        return redirect('http://45.32.123.218/events/'.$request->slug);
    }
    
    // public function redirectIntoJejakKhilafah(Request $request, $slug)
    // {
    
    //      $ip = [
         
    //      '139.162.16.133',
    //      '172.104.174.163',
    //      '172.105.112.186',
    //      '139.162.23.161',
    //      '139.162.7.183',
    //      '139.162.10.211',
    //      '172.104.162.67',
    //      '139.162.35.12'
    //      ];
         
    //      $counter = Cache::get('ip', 0);

    //      Cache::forever('ip', $counter+1);
         
    //      $selected =  $ip[$counter % count($ip)];
         
    //      return redirect('http://'.$selected.'/p/'.$slug);
    // }

    public function redirectIntoJejakKhilafah(Request $request, $slug)
    {
    
      

         return redirect('http://bit.ly/film-jkdn');
    }

    public function test()
    {
        $data  = file_get_contents(base_path().'/public/list-ip.txt',true);
 
        $ip = preg_split('/\n+/', $data);
        
        $counter = Cache::get('ip', 0);

        Cache::forever('ip', $counter+1);
        
         $selected =  $ip[$counter % count($ip)];
     
         return $this->pingAddress($selected);

    }

    function pingAddress($ip) {
        $pingresult = exec("ping -n 1 $ip", $outcome, $status);
        return $outcome;
        // echo $outcome;
        // if (0 == $status) {
        //     $status = "alive";
        // } else {
        //     $status = "dead";
        // }
        // echo "The IP address, $ip, is  ".$status;
    }
}
