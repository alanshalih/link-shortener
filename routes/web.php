<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {

	if($request->getHttpHost() == 'jkdn.online'){

		$data  = file_get_contents(base_path().'/public/list-ip.txt',true);

		$ip = preg_split('/\n+/', $data);
		
		$counter = Cache::get('ip', 0);

		Cache::forever('ip', $counter+1);
		
		 $selected =  $ip[$counter % count($ip)];
		
		return redirect('http://'.$selected);
	
	}else{
		  return view('welcome');
	}

	
});

Route::get('/test','LinkController@test');

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'LinkController@index')->name('home');
	Route::resource('/link','LinkController');
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::get('/s/{slug}', 'LinkController@redirectIntoRamadhankita');
Route::get('/p/{slug}', 'LinkController@redirectIntoJejakKhilafah');
Route::post('/checklink','LinkController@checkLink');
Route::get('/{slug}', 'LinkController@show');


